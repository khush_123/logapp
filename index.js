/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Demoapicall from './Demoapicall';


AppRegistry.registerComponent(appName, () => Demoapicall);
