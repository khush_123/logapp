import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

export default class Demoapicall extends React.Component {
  constructor(props) {
    super(props);
    console.log('called constructor');

    this.state = {
      loading: true,
      source: null,
    };
  }

  componentDidMount() {
    console.log('component did mount');

    return fetch('https://reqres.in/api/login')
      .then(response => response.json())
      .then(responseJson => {
        console.log('getting data from fetch');
        this.setState({
          loading: false,
          source: responseJson.data,
        });
      })
      .catch(error => {
        console.log('catch error');
      });
  }

  render() {
    console.log('called error');

    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <Text>Content is being loaded!!!</Text>
          <ActivityIndicator />
        </View>
      );
    } else {
      console.log('content loaded!!!');

      let data = this.state.source.map((val, key) => {
        return (
          <View key={key} style={styles.item}>
            <Text style={{fontSize: 20, color: 'blue'}}>{val.id}</Text>
            <Text style={{fontSize: 20, color: 'green'}}>{val.email}</Text>
            <Text style={{fontSize: 20, color: 'red'}}>
              {val.first_name + ' ' + val.last_name}
            </Text>
          </View>
        );
      });

      return <View style={styles.container}>{data}</View>;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    margin: 10,
  },
});
